import React from 'react';
import ReactDOM from 'react-dom/client';
import './styles.scss'
import App from './App';
import { CookiesProvider } from 'react-cookie';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <CookiesProvider defaultSetOptions={{ path: '/' }}>
    <React.StrictMode>
      <App />
    </React.StrictMode>
  </CookiesProvider>
);

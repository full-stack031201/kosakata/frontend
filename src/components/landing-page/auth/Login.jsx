import { faLock, faUser } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useState } from "react";
import { CSSTransition } from 'react-transition-group';
import Swal from "sweetalert2";

const Login = () => {
    const [ openForm, setOpenForm ] = useState(false);
    const [ loadingSend, setLoadingSend ] = useState(false);
    const [ formData, setFormData ] = useState({
        username: '',
        password: '',
    });
    const [errorForm, setErrorForm] = useState(null)

    const submitForm = (e) => {
        e.preventDefault();
        setErrorForm(null)
        setLoadingSend(true)
        setTimeout(() => {
            const isValid = __rules();
            if (isValid) {
                console.log(formData);
                Swal.fire({
                    title: 'Asik Login Berhasil',
                    text: 'Yee, selamat datang sobat kosan',
                    icon: 'success',
                    confirmButtonText: 'Ok'
                })
            }
            setLoadingSend(false)
        }, 1000);
    }

    const __rules = () => {
        const errors = {};

        if(!formData.username) errors.username = 'Username harus diisi';
        if(!formData.password) errors.password = 'Password harus diisi';

        setErrorForm(errors);
        return Object.keys(errors).length === 0;
    }

    return (
        <div className="col-md-6">
            <div className="text-success text-center bg-primary shadow-lg p-md-4 p-3 rounded-4 m-3">
                <h6 className="mb-0">Sudah punya akun?</h6>
                <button className="btn btn-success mt-3" onClick={() => setOpenForm(!openForm)}>
                    Login Disini
                </button>
            </div>
            <CSSTransition
                in={openForm}
                timeout={300}
                classNames="slide"
                unmountOnExit
            >
                <div className="bg-primary rounded-3 p-3 m-3 text-success shadow-lg">
                    <div className="mx-lg-5 px-lg-5 px-3 my-3 py-3 border border-success rounded-3">
                        <form className="text-start" onSubmit={submitForm}>
                            <legend><strong>Form Login</strong></legend>
                            <div className="form-group mb-2">
                                <div className="position-relative">
                                    <FontAwesomeIcon icon={faUser} className="position-absolute" style={{ top: '48%', transform: 'translateY(-50%)', left: '10px' }} />
                                    <input value={formData.username} onChange={(e) => setFormData({...formData, username: e.target.value})} id="username" type="text" className="bg-transparent text-success py-2 pe-3 w-100 border-top-0 border-start-0 border-end-0 border-bottom-2 border-success" placeholder="Email/Username" style={{ paddingLeft: '35px' }} />
                                </div>
                                {errorForm  && errorForm.username && (
                                    <small className="text-danger"><em>{errorForm.username}</em></small>
                                )}
                            </div>
                            <div className="form-group mb-2">
                                <div className="position-relative">
                                    <FontAwesomeIcon icon={faLock} className="position-absolute" style={{ top: '48%', transform: 'translateY(-50%)', left: '10px' }} />
                                    <input value={formData.password} onChange={(e) => setFormData({...formData, password: e.target.value})} id="password" type="password" className="bg-transparent text-success py-2 pe-3 w-100 border-top-0 border-start-0 border-end-0 border-bottom-2 border-success" placeholder="Password" style={{ paddingLeft: '35px' }} />
                                </div>
                                {errorForm  && errorForm.password && (
                                    <small className="text-danger"><em>{errorForm.password}</em></small>
                                )}
                            </div>
                            <div className="form-group mb-2">
                                <button type={loadingSend ? 'button' : 'submit'} className={`btn ${loadingSend ? 'btn-outline-success' : 'btn-success'} w-100`}>{ loadingSend ? 'Loading ... ' : 'Masuk'}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </CSSTransition>
        </div>
    )
}

export default Login;

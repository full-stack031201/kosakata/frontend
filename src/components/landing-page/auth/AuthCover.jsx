import Login from "./Login";
import Register from "./Register";

const AuthCover = () => {
    return(
        <div className="position-relative" style={{ zIndex: 1 }}>
            <div className="container py-md-5 py-3 my-md-5 my-3">
                <div className="d-md-flex align-items-center justify-content-center" style={{ minHeight: '40vh' }}>
                    <div className="col-12 d-md-flex">
                        <Login />
                        <Register />
                    </div>
                </div>
            </div>
        </div>
    )
}
export default AuthCover;
import { faEnvelope, faLock, faPhone, faUser, faUserNinja } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useState } from "react";
import { CSSTransition } from 'react-transition-group';

const Register = () => {
    const [openForm, setOpenForm] = useState(false);
    return(
        <div className="col-md-6" >
            <div className="text-info text-center bg-white shadow-lg p-md-4 p-3 rounded-4 m-3">
                <h6 className="mb-0">Belum punya akun?</h6>
                <button className="btn btn-info mt-3" onClick={() => setOpenForm(!openForm)}>
                    Daftar Disini
                </button>
            </div>
            <CSSTransition
                in={openForm}
                timeout={300}
                classNames="slide"
                unmountOnExit
            >
                <div className="bg-white rounded-3 p-3 m-3 text-info shadow-lg">
                    <div className="mx-lg-5 px-lg-5 px-3 my-3 py-3 border border-info rounded-3">
                        <form className="text-start">
                            <legend><strong>Form Register</strong></legend>
                            <div className="form-group mb-2">
                                <div className="position-relative">
                                    <FontAwesomeIcon icon={faUser} className="position-absolute" style={{ top: '48%', transform: 'translateY(-50%)', left: '10px' }} />
                                    <input id="username" type="text" className="bg-transparent py-2 pe-3 w-100 border-top-0 border-start-0 border-end-0 border-bottom-2 border-info" placeholder="Username" style={{ paddingLeft: '35px' }} />
                                </div>
                            </div>
                            <div className="form-group mb-2">
                                <div className="position-relative">
                                    <FontAwesomeIcon icon={faEnvelope} className="position-absolute" style={{ top: '48%', transform: 'translateY(-50%)', left: '10px' }} />
                                    <input id="email" type="text" className="bg-transparent py-2 pe-3 w-100 border-top-0 border-start-0 border-end-0 border-bottom-2 border-info" placeholder="Email" style={{ paddingLeft: '35px' }} />
                                </div>
                            </div>
                            <div className="form-group mb-2">
                                <div className="position-relative">
                                    <FontAwesomeIcon icon={faUserNinja} className="position-absolute" style={{ top: '48%', transform: 'translateY(-50%)', left: '10px' }} />
                                    <input id="name" type="text" className="bg-transparent py-2 pe-3 w-100 border-top-0 border-start-0 border-end-0 border-bottom-2 border-info" placeholder="Nama" style={{ paddingLeft: '35px' }} />
                                </div>
                            </div>
                            <div className="form-group mb-2">
                                <div className="position-relative">
                                    <FontAwesomeIcon icon={faPhone} className="position-absolute" style={{ top: '48%', transform: 'translateY(-50%)', left: '10px' }} />
                                    <input id="phone_number" type="number" className="bg-transparent py-2 pe-3 w-100 border-top-0 border-start-0 border-end-0 border-bottom-2 border-info" placeholder="No. Hp" style={{ paddingLeft: '35px' }} />
                                </div>
                            </div>
                            <div className="form-group mb-2">
                                <div className="position-relative">
                                    <FontAwesomeIcon icon={faLock} className="position-absolute" style={{ top: '48%', transform: 'translateY(-50%)', left: '10px' }} />
                                    <input id="password" type="password" className="bg-transparent py-2 pe-3 w-100 border-top-0 border-start-0 border-end-0 border-bottom-2 border-info" placeholder="Kata Sandi" style={{ paddingLeft: '35px' }} />
                                </div>
                            </div>
                            <div className="form-group mb-2">
                                <div className="position-relative">
                                    <FontAwesomeIcon icon={faLock} className="position-absolute" style={{ top: '48%', transform: 'translateY(-50%)', left: '10px' }} />
                                    <input id="password_confirmation" type="password" className="bg-transparent py-2 pe-3 w-100 border-top-0 border-start-0 border-end-0 border-bottom-2 border-info" placeholder="Ulang Kata Sandi" style={{ paddingLeft: '35px' }} />
                                </div>
                            </div>
                            <div className="form-group mb-2">
                                <button className="btn btn-info w-100">Daftar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </CSSTransition>
        </div> 
    )
}
export default Register;
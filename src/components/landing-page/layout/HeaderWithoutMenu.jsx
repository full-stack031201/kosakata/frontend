import { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch, faTimesRectangle } from '@fortawesome/free-solid-svg-icons';

const HeaderComponentWithoutMenu = () => {
    const [ openSearch, setOpenSearch ] = useState(false)


    return (
        <>
            <div 
                className={`position-fixed top-0 left-0 w-100 vh-100 bg-dark ${openSearch ? `d-flex` : `d-none`} align-items-center justify-content-center d-md-none`}
                style={{zIndex: 99999}}>
                    <button className='btn position-absolute top-0 right-0' onClick={() => setOpenSearch(false)}>
                        <h1><FontAwesomeIcon icon={faTimesRectangle} className='text-white' /></h1>
                    </button>
                    <form className='position-relative col-11'>
                        <input 
                            type='text' 
                            className='border-0 border-bottom border-white form-control bg-transparent rounded-0 shadow-0 search-input-menu text-white' 
                            placeholder='Ayo cari kostan kamu disini'
                            style={{ paddingRight: '2rem' }}
                            required />
                        <button className='position-absolute right-0 middle bg-transparent border-0 text-white'>
                            <FontAwesomeIcon icon={faSearch} />
                        </button>
                    </form>
            </div>
            <nav className='navbar navbar-menu navbar-expand-lg bg-transparent w-100'>
                <div className="container d-flex align-items-center justify-content-center">
                    <div className='col-md-6 text-start me-auto'>
                        <a href="/">
                            <img src={'/assets/images/logo.png'} alt="" />
                            <img src={'/assets/images/kosakata.png'} alt="" className='d-md-inline d-none' />
                        </a>
                    </div>
                    <div className="col-md-6">
                        <div className='d-flex align-items-center justify-content-end w-100'>
                            <form className='position-relative col-md-8 d-md-block d-none'>
                                <input 
                                    type='text' 
                                    className='border-0 border-bottom border-dark form-control bg-transparent pe-4 rounded-0 shadow-0' 
                                    placeholder='Ayo cari kostan kamu disini'
                                    required />
                                <button className='position-absolute right-0 middle bg-transparent border-0'>
                                    <FontAwesomeIcon icon={faSearch} />
                                </button>
                            </form>
                            <div className='d-md-none me-2'>
                                <button className="btn" onClick={() => setOpenSearch(true)}>
                                    <FontAwesomeIcon icon={faSearch} />
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </>
    )
}
export default HeaderComponentWithoutMenu;
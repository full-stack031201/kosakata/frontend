import { useLocation } from "react-router";
import FooterComponent from "./FooterComponent";
import HeaderComponent from "./HeaderComponent";
import ScrollTop from "./ScrollTop";
import HeaderComponentWithoutMenu from "./HeaderWithoutMenu";

export default function LayoutLandingPage ({ children }) {

    const location = useLocation()

    return(
        <div className="bg-white">
            <ScrollTop />
            { location.pathname === '/' ? <HeaderComponent /> : <HeaderComponentWithoutMenu />}
                {children}
            <FooterComponent />
        </div>
    )
}
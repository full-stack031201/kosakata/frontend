import Home from "../views/landing-page/home/Home";
import KebijakanPrivasi from "../views/landing-page/kebijakan-privasi/KebijakanPrivasi";

const menuHome = [
    {path: '/', element: <Home />},
    {path: '/kebijakan-privasi', element: <KebijakanPrivasi />},
]


const HomeRouter = [...menuHome];

export default HomeRouter;

import { useEffect, useState } from "react";
import LayoutLandingPage from "../../../components/landing-page/layout/LayoutComponent";

export default function KebijakanPrivasi(){
    const [ privacy, setPrivacy ] = useState('Lorem ipsum dolor, sit amet consectetur adipisicing elit. Eligendi iste accusantium dolor ab quibusdam culpa soluta voluptatibus cupiditate ducimus blanditiis, minus similique voluptatem iure reprehenderit tenetur aliquam. Ab, laboriosam! Blanditiis.');
    useEffect(() => {
        setPrivacy('Lorem ipsum dolor, sit amet consectetur adipisicing elit. Eligendi iste accusantium dolor ab quibusdam culpa soluta voluptatibus cupiditate ducimus blanditiis, minus similique voluptatem iure reprehenderit tenetur aliquam. Ab, laboriosam! Blanditiis.')
    }, [])
    return(
        <>
            <LayoutLandingPage>
            <div className="bg-primary position-relative">
                <div className="container position-relative z-2">
                    <div className="row">
                        <div className="col-12">
                            <div className="pt-md-5 pt-3 pb-3">
                                <h1 className="text-success text-center m-0">KEBIJAKAN PRIVASI</h1>
                            </div>
                        </div>
                        <div className="col-12" style={{ textAlign: 'justify' }}>
                            <div className="pb-md-5 pb-3 pt-3">
                                {privacy}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </LayoutLandingPage>
        </>
    )
}
import LayoutLandingPage from "../../../components/landing-page/layout/LayoutComponent";
import Circle1 from '../../../assets/images/auth-circle-1.png';
import AuthCover from "../../../components/landing-page/auth/AuthCover";

export default function Authentification(){
    return(
        <>
            <img src={Circle1} alt="" className="position-absolute top-0" width={'200px'} style={{ zIndex: 0 }} />
            <LayoutLandingPage>
                <AuthCover />
            </LayoutLandingPage>
        </>
    )
}